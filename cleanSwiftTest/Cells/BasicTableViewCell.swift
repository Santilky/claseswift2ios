//
//  BasicTableViewCell.swift
//  cleanSwiftTest
//
//  Created by Santiago Linietsky on 25/01/2022.
//

import UIKit
protocol BasicCellModel {
    var title: String {get set}
    var target: Any {get set}
    var selector: Selector {get set}
}
class BasicTableViewCell: UITableViewCell {
    static let identifier: String = "BasicTableViewCell"
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    var texto: String?
    @IBOutlet weak var tableViewLabel: UILabel!
    @IBOutlet weak var cellButton: UIButton!
    
    public func setup(data: BasicCellModel) {
        tableViewLabel.text = data.title
        cellButton.addTarget(data.target, action: data.selector, for: .touchDown)
    }
}
