//
//  IntroScreenViewModel.swift
//  cleanSwiftTest
//
//  Created by Santiago Linietsky on 25/01/2022.
//

import Foundation
struct LlamadoDeApi: Codable {
    var id: Int
    var nombre: String
    var apellido: String
    var dni: Int
}
class IntroScreenViewModel {
    init() {
        fillArrayWithModels()
    }
    var basicModels: [IntroScreen.BasicViewCellModel] = []
    
    private func fillArrayWithModels() {
        let model1: LlamadoDeApi = .init(id: 1, nombre: "Juan", apellido: "Bautista", dni: 3434344)
        let model2: LlamadoDeApi = .init(id: 2, nombre: "Jazmin", apellido: "Perez", dni: 3434344)
        let model3: LlamadoDeApi = .init(id: 3, nombre: "Soledad", apellido: "Bauman", dni: 3434344)
        basicModels.append(.init(model: .init(title: "\(model1.nombre) \(model1.apellido)", target: self, selector: #selector(self.onCellButtonClick))))
        basicModels.append(.init(model: .init(title: "\(model2.apellido) \(model2.dni)", target: self, selector: #selector(self.onCellButtonClick))))
        basicModels.append(.init(model: .init(title: "\(model1.id + model2.id + model3.id)", target: self, selector: #selector(self.onCellButtonClick))))
        basicModels.append(.init(model: .init(title: "una", target: self, selector: #selector(self.onCellButtonClick))))
        basicModels.append(.init(model: .init(title: "celula", target: self, selector: #selector(self.onCellButtonClick))))
        basicModels.append(.init(model: .init(title: "y", target: self, selector: #selector(self.onCellButtonClick))))
        basicModels.append(.init(model: .init(title: "etc...", target: self, selector: #selector(self.onCellButtonClick))))
    }
    @objc func onCellButtonClick() {
        print("Se hizo click!")
    }
}
