//
//  cleanSwiftTestTests.swift
//  cleanSwiftTestTests
//
//  Created by Santiago Linietsky on 23/10/2021.
//

import XCTest
@testable import cleanSwiftTest

class cleanSwiftTestTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        let result = 1 + 1
        XCTAssert(result == 2)
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
